FROM alpine:latest

VOLUME ["/data"]

LABEL version="1.0" maintainer="Simon Schäfer <simon.schaefer@koeln.de>" description="This container contains a cron'd certbot configured to get a wildcard certificate from duckdns"

RUN apk --no-cache add certbot curl

# fancy.duckdns.org
ENV DUCKDNS_SUBDOMAIN=fancy
ENV DUCKDNS_TOKEN=deadbeaf
ENV ACCOUNT_INFO_EMAIL=<yourmail>@example.com
ENV CERT_DOMAIN=example.com

RUN mkdir -p /scripts /data

COPY files/handleEntry.sh /scripts/cleanupEntry.sh
COPY files/handleEntry.sh /scripts/createEntry.sh
COPY files/request.sh /scripts/request.sh
COPY files/start.sh /scripts/start.sh
RUN chmod +x /scripts/*.sh

RUN ln -sf /scripts/request.sh /etc/periodic/daily/update-certbot

CMD ["/scripts/start.sh"]
