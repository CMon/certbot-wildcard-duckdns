#!/bin/sh

me=`basename "$0"`
URL="https://www.duckdns.org/update?domains=${DUCKDNS_SUBDOMAIN}.duckdns.org&token=${DUCKDNS_TOKEN}&txt=${CERTBOT_VALIDATION}"

if [ "$me" == "cleanupEntry.sh" ]
then
    curl "${URL}&clear=true"
elif [ "$me" == "createEntry.sh" ]
then
    curl "${URL}"
else
    echo "unknown script : ${me}"
fi

