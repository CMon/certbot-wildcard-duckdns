# Purpose

The purpose of this container is to have the ability to generate wildcard certificates without having to expose the server to the internet.
Additionaly it supports all kind of DNS hosters since its not using an api of the hoster. This can be necessary if you do not want to risk
loosing the access data to your main dns provider if your docker get hacked. Or if your hoster does not provide any kind of remote usable api
to let certbot do the necessary challenge changes.

In contrast to all other containers I could find, this container makes a difference between the domain it is requesting the certificate for
and the domain where does the challenge.

# Configuration
Example domain: `example.com`
Example subdomain: `duck-example`

1. Get an account at duckdns and setup your `duck-example`-subdomain
2. Store the API Tjoken from duckdns
3. Go to your main DNS provider that holds the `example.com` domain
4. change the `_acme-challenge.example.com` CNAME to be directed to your duckdns `_acme-challenge` entry, for example: `_acme-challenge.example.com CNAME _acme-challenge.duck-example.duckdns.org.`

## Optional (if you are behind a router):
* add a dns rebind exception for `example.com` and `*.example.com`

## Test your Setup
run: `nslookup -type=txt _acme-challenge.example.de`
This should return a redirect to the duckdns entry you just set up

# Setup Docker container:

```
docker run -it \
      -e ACCOUNT_INFO_EMAIL=yourmail@example.com \
      -e CERT_DOMAIN=example.com \
      -e DUCKDNS_SUBDOMAIN=duck-example \
      -e DUCKDNS_TOKEN=xxx-x-x-x-xxx \
      -v "data":"/data":rw \
      cmon23/certbot-wildcard-duckdns
```

Optional environment variable:
`LETS_ENCRYPT_ACME_URL` can be used to overload the default ACME URL.

This is Useful if you are testing and want to use the staging server.
In this case add: `-e LETS_ENCRYPT_ACME_URL="https://acme-staging-v02.api.letsencrypt.org/directory"` to your call

The certificates can then be found inside your `<data>/conf/live`.

# Triggering other updates after a renewed certificate

Place a executable file inside the folder `<data>/conf/renewal-hooks/deploy/` that can be executed by `/bin/sh`. If you need more info regarding the hooks have a look into the certbot documentation.
